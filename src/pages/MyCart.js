
import { useContext, useState, useEffect, Fragment } from "react";
import UserContext from '../UserContext';
import { Navigate, useNavigate } from "react-router-dom";
import { Table, Container, Button } from "react-bootstrap";
import Swal from "sweetalert2";


export default function MyCart(){

    const { user } = useContext(UserContext);
    const [ cart, setCart ] = useState([]);
    const [ totalAmount, setTotalAmount ] = useState(0);
    const navigate = useNavigate();

    const fetchData = () =>{
		fetch(`${process.env.REACT_APP_API_URL}/carts/cart`, {
			headers:{
				"Authorization": `Bearer ${ localStorage.getItem('token') }`
			}
		})
		.then(res => res.json())
		.then(data => {
		data.map(cart => {
                setCart(cart.cartProducts.map(product => {
                    return(  
                        <tr key={product._id} className="d-flex">
                            <td className="text-light col-4">{product.productName}</td>
                            <td className="text-light col-4">{product.quantity}</td>
                            <td className="text-light col-4">{product.subTotal}</td>
                        </tr>   
                    )
                }))
                setTotalAmount(cart.cartTotalAmount);
			});
		});        
	}
    useEffect(()=>{
		fetchData();
	}, [])

    const checkout = () => {
		fetch(`${ process.env.REACT_APP_API_URL }/carts/checkout`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			}
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				Swal.fire({
					title: "Successfully Ordered",
					icon: "success",
					text: "Orders successfully placed."
				});
				fetchData();
			}
			else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});
			}
		});
	}

    const clearCart = () => {
        fetch(`${ process.env.REACT_APP_API_URL }/carts/deleteItems`, {
            method: "DELETE",
            headers:{
                "Content-Type": "application/json",
                "Authorization": `Bearer ${ localStorage.getItem("token") }`
            }
        })
        .then(res => res.json())
        .then(data => {
            if(data){
                Swal.fire({
                    title: "Successful",
                    icon: "success",
                    text: `Cart items has been deleted`
                });
                fetchData();
            }
            else{
                Swal.fire({
                    title: "Delete Unsuccessful",
                    icon: "error",
                    text: "Something went wrong. Please try again later!"
                });
            }
        })
    }

    return(
        (user.id !== null)?
            <Container>
                <h1 className="text-center pt-3 pb-3">My Cart</h1>
                <Table striped bordered hover className="bg-dark text-light border">
                    <thead>
                        <tr className="text-center d-flex">
                        <th className="col-4">Product Name</th>
                        <th className="col-4">Quantity</th>
                        <th className="col-4">Subtotal</th>
                        </tr>
                    </thead>
                    <tbody>
                        {cart}
                    </tbody>
                    <div className="border">
                        <h5 className="bg-dark text-light pt-2 ps-2">Total Amount: ₱ {totalAmount}.00</h5>
                    </div>
                </Table>

                <div className="text-end">
                    {(cart.length <= 0)?
                        <Fragment>
                            <Button variant="success"  size="lg" onClick={() => checkout()} disabled>Checkout</Button>
                            <Button variant="success ms-3"  size="lg" onClick={() => clearCart()} disabled>Clear Cart</Button>
                        </Fragment>
                    :
                        <Fragment>
                            <Button variant="success"  size="lg" onClick={() => checkout()}>Checkout</Button>
                            <Button variant="success ms-3"  size="lg" onClick={() => clearCart()}>Clear Cart</Button>
                        </Fragment>
                    }
                </div>
            </Container>
        :
        <Navigate to="/" />
    )
}