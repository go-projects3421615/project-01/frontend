
import { Container, Card, Button, Form } from "react-bootstrap";
import { useParams, useNavigate } from "react-router-dom"
import { useState, useEffect, useContext } from "react";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function ViewProduct(){
	const { user } = useContext(UserContext);
	const { productId } = useParams();

	const navigate = useNavigate();

	const [ name, setName ] = useState('');
	const [ price, setPrice ] = useState(0);
	const [ quantity, setQuantity ] = useState(1);

	useEffect( () => {

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {

			setName(data.name);
			setPrice(data.price);
		});
	}, [productId])

	const order = (productId) =>{
		fetch(`${process.env.REACT_APP_API_URL}/carts/addToCart`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				productId: productId,
                quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data){
				Swal.fire({
					title: "Successful",
					icon: "success",
					text: "Product added to your cart."
				});
				navigate("/");
			}
			else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});
			}
		});
	}

	useEffect(() => {
		if(quantity <= 0){
			Swal.fire({
				title: "Invalid Quantity",
				icon: "error",
				text: "Please try again."
			});
			setQuantity(1);
		}
	})

	return(
			<Container className="pt-5">
				<Card id="card-view" className='col-lg-4 col-md-6'>
					<Card.Body className="text-center">
						<Card.Title>{name}</Card.Title>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>PhP {price}</Card.Text>
						<Card.Subtitle className="pb-3">Quantity</Card.Subtitle>
							<div className="col-4 offset-4">
								<Form.Control 
									type="number"
									placeholder="Enter Product Price" 
									value = {quantity}
									onChange={event => setQuantity(event.target.value)}
									required
								/>
							</div>
						<Card.Text></Card.Text>
						<Button variant="primary"  size="lg" onClick={() => order(productId)}>Add to Cart</Button>
					</Card.Body>		
				</Card>
			</Container>
	)
}