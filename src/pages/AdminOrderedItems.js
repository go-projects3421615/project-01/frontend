import { useContext, useState, useEffect, Fragment } from "react";
import { Container, Button, Card } from "react-bootstrap";
import { Navigate, Link } from "react-router-dom";
import UserContext from "../UserContext";


export default function AdminOrderedItems(){

    const { user } = useContext(UserContext);

    const [ allOrders, setAllOrders ] = useState([]);

    const fetchData = () =>{
		fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(async data => {

		await setAllOrders(await data.map(product => {

                return (
                    <div id="card-view" className="col-lg-4 col-md-6 pt-2 pb-3">
                        <Card key={product._id} className='text-dark bg-muted p-3'>
                        <Card.Body>
                            <Card.Title>Product Name: {product.name}</Card.Title>
                            <Card.Subtitle>Price: ₱{product.price}</Card.Subtitle>
                            <br/>
                            <div className="row">
                                <Card.Subtitle className="col-8 col-lg-9">Who Ordered</Card.Subtitle>
                                <Card.Subtitle className="col-4 col-lg-3">Quantity</Card.Subtitle>
                            </div>
                            
                            <div className="row">
                                <div className="col-10">{product.orders.map(order => {
                                    return (
                                        <Fragment>
                                            <>{order.userEmail}</>       
                                            <br/>
                                        </Fragment>
                                    )
                                })}</div>
                                <div className="col-2">{product.orders.map(order => {
                                    return (
                                        <Fragment>
                                            <>{order.quantity}</>       
                                            <br/>
                                        </Fragment>
                                    )
                                })}</div>
                            </div>
                            
                        </Card.Body>
                        </Card>
                    </div>
                )
			}));
		});
	}
	useEffect(()=>{
		fetchData();
	}, [])

    return(
        (user.isAdmin === 'true')
        ?
        <Container>
            <div className="pt-5 mb-3 text-center">
				<h1>Admin Ordered Products</h1>
				<Button variant="primary" className="mx-2" as={Link} to="/admin">Back to Dashboard</Button>
			</div>

            <div className="row">
                {allOrders}
            </div>
        </Container>
        :
        <Navigate to="/" />
    )
}