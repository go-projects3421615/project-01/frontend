
import { useContext, useState, useEffect } from "react";
import UserContext from '../UserContext';
import { Navigate, Link } from "react-router-dom";
import { Button, Table, Modal, Form, Container } from "react-bootstrap";
import Swal from "sweetalert2";


export default function AdminDashboard(){

	const { user } = useContext(UserContext);

	const [allProducts, setAllProducts] = useState([]);
	const [productId, setProductId] = useState("");
	const [name, setName] = useState("");
    const [price, setPrice] = useState(0);
    const [isActive, setIsActive] = useState(false);

    // State for Add/Edit Modal
    const [showAdd, setShowAdd] = useState(false);
	const [showEdit, setShowEdit] = useState(false);

	// To control the add product modal pop out
	const openAdd = () => setShowAdd(true);
	const closeAdd = () => setShowAdd(false);


	const openEdit = (id) => {
		setProductId(id);
		fetch(`${ process.env.REACT_APP_API_URL }/products/${id}`)
		.then(res => res.json())
		.then(data => {
			setName(data.name);
			setPrice(data.price);
		});
		setShowEdit(true)
	};
	const closeEdit = () => {
	    setName('');
	    setPrice(0);
		setShowEdit(false);
	};


	const fetchData = () =>{
		fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(async data => {

		await setAllProducts(await data.map(product => {
				return (
					<tr key={product._id} className="d-flex">
						<td className="text-light col-4">{product.name}</td>
						<td className="text-light col-4">{product.price}</td>
						<td className="text-light col-4 text-center">
							<Button variant="secondary" size="sm" className="mx-1" onClick={() => openEdit(product._id)}>Edit Product</Button>
						</td>
					</tr>
				)
			}));
		});
	}
	useEffect(()=>{
		fetchData();
	}, [])


	const addProduct = (e) =>{
		    e.preventDefault();
		    fetch(`${process.env.REACT_APP_API_URL}/products/addProduct`, {
		    	method: "POST",
		    	headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
				    name: name,
				    price: price
				})
		    })
		    .then(res => res.json())
		    .then(data => {

		    	if(data){
		    		Swal.fire({
		    		    title: "New Product Added",
		    		    icon: "success",
		    		    text: `${name} is now added`
		    		});
		    		// To automatically add the update in the page
		    		fetchData();
		    		// Automatically closed the modal
		    		closeAdd();
		    	}
		    	else{
		    		Swal.fire({
		    		    title: "Error!",
		    		    icon: "error",
		    		    text: `Something went wrong. Please try again later!`
		    		});
		    		closeAdd();
		    	}
		    })
		    setName('');
		    setPrice(0);
	}
	const editProduct = (e) =>{
		    e.preventDefault();
		    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/update`, {
		    	method: "PUT",
		    	headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
				    name: name,
				    price: price
				})
		    })
		    .then(res => res.json())
		    .then(data => {

		    	if(data){
		    		Swal.fire({
		    		    title: "Product Successfully Updated",
		    		    icon: "success",
		    		    text: `${name} is now available`
		    		});
		    		fetchData();
		    		closeEdit();
		    	}
		    	else{
		    		Swal.fire({
		    		    title: "Error!",
		    		    icon: "error",
		    		    text: `Something went wrong. Please try again later!`
		    		});
		    		closeEdit();
		    	}
		    })
		    setName('');
		    setPrice(0);
	} 

	// Submit button validation for add/edit a product
	useEffect(() => {
        if(name !== "" && price > 0){
            setIsActive(true);
        } 
		else{
            setIsActive(false);
        }
    }, [name, price]);
	
	return(
		(user.isAdmin === 'true')
		?
		<Container>
			<div className="pt-5 mb-3 text-center">
				<h1>Admin Dashboard</h1>
				<Button variant="primary" className="mx-2" onClick={openAdd}>Add Product</Button>
				<Button variant="primary" className="mx-2" as={Link} to="/admin/orders">Ordered Products</Button>
			</div>

			<Table striped bordered hover className="bg-dark text-light">
		      	<thead>
					<tr className="d-flex text-center">
						<th className="col-4">Product Name</th>
						<th className="col-4">Price</th>
						<th className="col-4">Actions</th>
					</tr>
		      	</thead>
		      	<tbody>
	        		{allProducts}
		      	</tbody>
		    </Table>

	        <Modal show={showAdd} fullscreen={true} onHide={closeAdd} >
				<Container className="d-flex-sm justify-content-center my-auto h-100">
					<Form onSubmit={e => addProduct(e)}>

						<Modal.Header closeButton>
							<Modal.Title>Add Product</Modal.Title>
						</Modal.Header>

						<Modal.Body>
							<Form.Group controlId="name" className="mb-3">
								<Form.Label>Product Name</Form.Label>
								<Form.Control 
									type="text" 
									placeholder="Enter Product Name" 
									value = {name}
									onChange={e => setName(e.target.value)}
									required
								/>
							</Form.Group>

							<Form.Group controlId="price" className="mb-3">
								<Form.Label>Price</Form.Label>
								<Form.Control 
									type="number" 
									placeholder="Enter Product Price" 
									value = {price}
									onChange={e => setPrice(e.target.value)}
									required
								/>
							</Form.Group>
						</Modal.Body>

						<Modal.Footer>
							{ isActive 
								? 
								<Button variant="primary" type="submit" id="submitBtn">
									Save
								</Button>
								: 
								<Button variant="danger" type="submit" id="submitBtn" disabled>
									Save
								</Button>
							}
							<Button variant="secondary" onClick={closeAdd}>
								Close
							</Button>
						</Modal.Footer>
					</Form>	
				</Container>
	    	</Modal>

			<Modal show={showEdit} fullscreen={true} onHide={closeEdit}>
				<Form onSubmit={e => editProduct(e)}>
					<Container>				
						<Modal.Header closeButton>
							<Modal.Title>Edit Product</Modal.Title>
						</Modal.Header>

						<Modal.Body>
							<Form.Group controlId="name" className="mb-3">
								<Form.Label>Product Name</Form.Label>
								<Form.Control 
									type="text" 
									placeholder="Enter Product Name" 
									value = {name}
									onChange={e => setName(e.target.value)}
									required
								/>
							</Form.Group>

							<Form.Group controlId="price" className="mb-3">
								<Form.Label>Price</Form.Label>
								<Form.Control 
									type="number" 
									placeholder="Enter Product Price" 
									value = {price}
									onChange={e => setPrice(e.target.value)}
									required
								/>
							</Form.Group>
						</Modal.Body>

						<Modal.Footer>
							{ isActive 
								? 
								<Button variant="primary" type="submit" id="submitBtn">
									Save
								</Button>
								: 
								<Button variant="danger" type="submit" id="submitBtn" disabled>
									Save
								</Button>
							}
							<Button variant="secondary" onClick={closeEdit}>
								Close
							</Button>
						</Modal.Footer>
					</Container>
				</Form>	
			</Modal>
		</Container>
		:
		<Navigate to="/" />
    )
}