
import { useContext, useState, useEffect } from "react";
import UserContext from '../UserContext';
import { Navigate } from "react-router-dom";
import { Table, Container } from "react-bootstrap";


export default function MyOrders(){

    const { user } = useContext(UserContext);
    const [ myOrders, setMyOrders ] = useState([]);

    const fetchData = () =>{
		fetch(`${process.env.REACT_APP_API_URL}/users/myOrders`, {
			headers:{
				"Authorization": `Bearer ${ localStorage.getItem("token") }`
			}
		})
		.then(res => res.json())
		.then(data => {

		setMyOrders(data.map(order => {
				return (
					<tr key={order._id} className="d-flex">
						<td className="text-light col-4">{order.productName}</td>
						<td className="text-light col-4">{order.quantity}</td>
						<td className="text-light col-4">{order.subTotal}</td>
					</tr>
				)
			}));
		});
	}
    useEffect(()=>{
		fetchData();
	}, [])

    return(
        (user.id !== null && user.email !== null)?
            <Container className="text-white">
                <h1 className="text-center pt-3 pb-3">My Orders</h1>
                <Table striped bordered hover className="bg-dark text-light border">
                    <thead>
                        <tr className="text-center d-flex">
                        <th className="col-4">Product Name</th>
                        <th className="col-4">Quantity</th>
                        <th className="col-4">Subtotal</th>
                        </tr>
                    </thead>
                    <tbody>
                        {myOrders}
                    </tbody>
                </Table>
            </Container>
        :
        <Navigate to="/" />
    )
}