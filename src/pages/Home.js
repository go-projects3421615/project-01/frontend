
import ProductCard from '../components/ProductCard';
import { useEffect, useState } from "react";
import { Container, Form } from 'react-bootstrap';


export default function Home(){

    const [products, setProducts] = useState([]);
    const [data, setData] = useState([]);
    const [filterValue, setfilterValue] = useState('');

    useEffect(() => {
        const fetchData = () => {fetch(`${process.env.REACT_APP_API_URL}/products/all`)
        .then(res => res.json())
        .then(data => {
            setData(data);
            setProducts(data);
            })
        }
    
    fetchData();
    }, [])
    
    const handleFilter = (event) => {
        if(event.target.value === ''){
            setData(products);
        }
        else{
            const filterResult = products.filter(item => item.name.toLowerCase().includes(event.target.value.toLowerCase()))
            setData(filterResult);
        }
        setfilterValue(event.target.value)
    }

    return (
        <div>
            <Container>
                <div className="pt-4 col-lg-4 offset-lg-4">
                    <Form.Control size="lg" className="mb-4" placeholder='Search' value={filterValue} onInput={(e) => handleFilter(e)} />
                </div>
                <div className='row'>
                    {
                        data.map(product => {
                            return(
                                <ProductCard key={product._id} productProp={product} />
                            )
                        })
                    }
                </div>
            </Container>
        </div>
    )
}