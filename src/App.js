
// Dependencies
import './App.css';

import { Container } from 'react-bootstrap';
import { UserProvider } from './UserContext';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router';
import { useState } from 'react';

import Home from './pages/Home';
import ViewProduct from './pages/ViewProduct';
import Register from './pages/Register';
import Login from './pages/Login';
import MyCart from './pages/MyCart';
import MyOrders from './pages/MyOrders';
import Logout from './pages/Logout';
import Error from './pages/Error';
import AppNavbar from './components/AppNavbar';
import AdminDashboard from './pages/AdminDashboard';
import AdminOrderedItems from './pages/AdminOrderedItems';



export default function App(){

  const [user, setUser] = useState({
      fullName: localStorage.getItem('fullName'),
      email: localStorage.getItem('email'),
      id: localStorage.getItem('id'),
      isAdmin: localStorage.getItem('isAdmin')
  })
  const unsetUser = () => {
      localStorage.clear();
  }

  return(
      <UserProvider value={{user, setUser, unsetUser}}>
          <Router>
              <Container fluid>
                  <AppNavbar />
                      <Routes>
                            <Route path="/" element={<Home />} />
                                <Route path="/products/:productId" element={<ViewProduct />} />
                            <Route path="/register" element={<Register />} />
                            <Route path="/login" element={<Login />} />
                            <Route path="/mycart" element={<MyCart />} />
                            <Route path="/orders" element={<MyOrders />} />
                            <Route path="/logout" element={<Logout />} />
                            <Route path="/admin" element={<AdminDashboard />} />
                            <Route path="/admin/orders" element={<AdminOrderedItems />} />
                            <Route path="*" element={<Error />} />
                      </Routes>
              </Container>
          </Router>
      </UserProvider>
  )
}

