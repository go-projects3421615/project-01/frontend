
// Dependencies
import { useContext, Fragment } from "react";
import { Container, Navbar, Nav, NavDropdown, Button, Form } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import UserContext from "../UserContext";


export default function AppNavbar(){

    const { user } = useContext(UserContext);

    return(
        <Navbar id="global-navbar" className="p-3 bg-dark" expand="lg">
            <Container>
                <Navbar.Brand className="ps-3 text-light" as={NavLink} to="/">Simple Add-to-Cart</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ms-auto">
                        <Nav.Link as={NavLink} to="/" className='pe-5 text-light'>Home</Nav.Link>
                        {(user.email !== null)?
                            <Fragment>
                                <Nav.Link as={NavLink} to="/orders" className='pe-5 text-light'>My Orders</Nav.Link>
                                <NavDropdown title={<span className="text-light">{user.email}</span>} id="nav-dropdown-logout">
                                {(user.isAdmin === 'true')?
                                    <Fragment>
                                        <NavDropdown.Item as={NavLink} to="/admin">Admin Dashboard</NavDropdown.Item>
                                        <NavDropdown.Item as={NavLink} to="/mycart">My Cart</NavDropdown.Item>
                                    </Fragment>
                                    :
                                    <NavDropdown.Item as={NavLink} to="/mycart">My Cart</NavDropdown.Item>               
                                }
                                <NavDropdown.Item as={NavLink} to="/logout">Logout</NavDropdown.Item>
                                </NavDropdown>
                            </Fragment>
							:
							<Fragment>
								<Nav.Link className="pe-5 text-light" as={NavLink} to="/login">Log In</Nav.Link>
								<Nav.Link className="pe-3 text-light" as={NavLink} to="/register">Sign Up</Nav.Link>
							</Fragment>
						}
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}