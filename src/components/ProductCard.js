
import{ Button, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom'
import UserContext from '../UserContext';
import { useContext } from 'react';

export default function ProductCard({productProp}) {

  const { user } = useContext(UserContext);
  const { _id, name, price } = productProp;

  return (
    <Card id="card-design" className='col-lg-3 col-md-6 p-3'>
    <Card.Body>
          <Card.Title>{name}</Card.Title>
          <Card.Subtitle>Price:</Card.Subtitle>
          <Card.Text>{price}</Card.Text>
          <div className='text-end'>
            { (user.id !== null) ?
              <Button className='border border-dark bg-light text-dark ps-4 pe-4' as={Link} to={`/products/${_id}`}>View Product</Button>
            :
              <Button className='border border-dark bg-light text-dark' disabled>Sign in to order</Button>
            }
          </div>
        </Card.Body>
      </Card>
  )
}
